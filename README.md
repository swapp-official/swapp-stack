# SWAPP #

Share and swap resources between members of the same community. This exchange is limited to food, health products, and medicines. These exchanges are conditioned to internal and security protocols. If you are a restaurant and you want to offer or make a donation of your leftover food, you can register in our application and help our community.

### Backend feautures ###

* Java Spring Boot
* Postgres DB
* Run the project: `mvn spring-boot:run`

### Frontend features ###

* React Native
* Expo
* Run the project:     
		`npm install -g expo`  
		`npm install`  
		`npm run`  

### DevOps feautures ###

* AWS
* Circle CI Pipeline
