package com.swapp.service.service;


import com.swapp.service.entity.ProductEntity;
import com.swapp.service.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductEntity> getProducts(){
        return productRepository.findAll();
    }

    public Optional<ProductEntity> getProduct(Long id){
        return productRepository.findById(id);
    }

    public ProductEntity createProduct(ProductEntity productEntity){
        return productRepository.save(productEntity);
    }

}
