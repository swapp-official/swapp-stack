package com.swapp.service.service;

import com.swapp.service.entity.UserEntity;
import com.swapp.service.entity.UserProductEntity;
import com.swapp.service.repository.UserProductRepository;
import com.swapp.service.repository.UserRepository;
import com.swapp.service.response.ProductResponse;
import com.swapp.service.response.UserProductResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserProductService {

    @Autowired
    private UserProductRepository userProductRepository;

    @Autowired
    private UserRepository userRepository;

    public List<UserProductResponse> getList(Long excludedUserId) {
        UserEntity userEntity = userRepository.getOne(excludedUserId);
        List<UserProductEntity> userProductEntities = userProductRepository.getListUserProductsExcludingUser(userEntity);
        Map<Long, List<UserProductEntity>> productsByUser = userProductEntities.stream()
            .collect(Collectors.groupingBy(u -> u.getUser().getId()));
        return productsByUser.entrySet().stream()
            .map(this::mapUserProductEntityToResponse)
            .collect(Collectors.toList());
    }


    /**
     * Method that creates a user-product response from an entry
     *
     * @param userEntry
     * @return
     */
    private UserProductResponse mapUserProductEntityToResponse(Map.Entry<Long, List<UserProductEntity>> userEntry) {
        UserProductResponse userProductResponse = new UserProductResponse();
        UserEntity userCurrentEntity = userEntry.getValue().get(0).getUser();
        userProductResponse.setUserAvatar(userCurrentEntity.getAvatar());
        userProductResponse.setUserName(userCurrentEntity.getNames());
        userProductResponse.setUserId(userCurrentEntity.getId());
        userProductResponse.setProducts(userEntry.getValue().stream().map(userProductEntity -> {

            ProductResponse productResponse = new ProductResponse();
            productResponse.setUserProductId(userProductEntity.getId());
            productResponse.setCategoryName(userProductEntity.getProduct().getCategory().getName());
            productResponse.setPhotos(userProductEntity.getProduct().getPhotos());
            productResponse.setQuantity(userProductEntity.getQuantity());
            productResponse.setProductName(userProductEntity.getProduct().getName());
            productResponse.setProductDescription(userProductEntity.getProduct().getDescription());
            productResponse.setUserProductExpireDate(userProductEntity.getExpireDate());
            productResponse.setUserProductComments(userProductEntity.getComments());

            return productResponse;
        }).collect(Collectors.toList()));
        return userProductResponse;
    }

    @Transactional
    public Integer updateQuantityWithAmount(Long userProductId, Integer amount) {
        return userProductRepository.updateQuantityByIdWithAmount(userProductId, amount);
    }

    @Transactional
    public UserProductEntity create(UserProductEntity userProductEntity) {
        return userProductRepository.save(userProductEntity);
    }
}
