package com.swapp.service.service;

import com.swapp.service.entity.UserEntity;
import com.swapp.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;

    public Optional<UserEntity> getUser(Long id){
        return userRepo.findById(id);
    }
}
