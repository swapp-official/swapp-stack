package com.swapp.service.service;

import com.swapp.service.entity.SwapEntity;
import com.swapp.service.entity.SwapProductEntity;
import com.swapp.service.entity.SwapStatusEntity;
import com.swapp.service.entity.UserEntity;
import com.swapp.service.repository.SwapProductRepository;
import com.swapp.service.repository.SwapRepository;
import com.swapp.service.repository.UserProductRepository;
import com.swapp.service.repository.UserRepository;
import com.swapp.service.request.CreateSwapRequest;
import com.swapp.service.response.SwapProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class SwapService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SwapRepository swapRepository;

    @Autowired
    private SwapProductRepository swapProductRepository;

    @Autowired
    private UserProductRepository userProductRepository;

    @Transactional
    public void create(CreateSwapRequest createSwapRequest) {
        SwapEntity swapEntityToCreate = new SwapEntity();

        UserEntity foreignUser = userRepository.getOne(createSwapRequest.getForeign().getUserId());
        swapEntityToCreate.setForeignUser(foreignUser);

        UserEntity ownUser = userRepository.getOne(createSwapRequest.getOwn().getUserId());
        swapEntityToCreate.setOwnUser(ownUser);
        swapEntityToCreate.setStatus(SwapStatusEntity.PENDING.getStatus());

        SwapEntity swapEntityCreated = swapRepository.save(swapEntityToCreate);

        List<SwapProductEntity> swapProductEntityList = createSwapRequest.getForeign().getSwapProductEntities();
        swapProductEntityList.addAll(createSwapRequest.getOwn().getSwapProductEntities());
        swapProductEntityList.forEach(swapProductEntity -> swapProductEntity.setSwap(swapEntityCreated));

        swapProductRepository.saveAll(swapProductEntityList);
    }

    @Transactional
    public int updateSwapRank(Long swapId, Double rank, String type) {
        if (type.equals("own")) {
            return swapRepository.updateOwnRank(swapId, rank);
        } else if (type.equals("foreign")) {
            return swapRepository.updateForeignRank(swapId, rank);
        }
        return 0;
    }

    @Transactional
    public Integer updateSwapStatus(Long swapId, SwapStatusEntity status) {
        Integer updated = swapRepository.updateStatus(swapId, status.getStatus());

        if (updated > 0 && status.equals(SwapStatusEntity.ACCEPTED)) {
            userProductRepository.updateProductQuantityBasedOnSwap(swapId);
        }

        return updated;
    }

    public Map<String, List<SwapEntity>> getList(Long userId){

        List<SwapEntity> swapsGetted = new ArrayList<>();
        List<SwapEntity> swapsSent = new ArrayList<>();
        List<SwapEntity> swapsFromDatabase = swapRepository.getListByUserId(userId);
        Map<String, List<SwapEntity>> swapsList = new HashMap<>();

        for( SwapEntity swap: swapsFromDatabase){
            if( swap.getOwnUser().getId().equals(userId) ){
                swapsSent.add(swap);
            } else {
                swapsGetted.add(swap);
            }
        }

        swapsList.put("getted", swapsGetted);
        swapsList.put("sent", swapsSent);

        return swapsList;
    }

    public Map<String, Object> getSwapDetails(Long swapId) {
        SwapEntity swap = swapRepository.getOne(swapId);

        if (swap == null) return null;

        List<SwapProductEntity> swapProductEntities = swapProductRepository.listSwapProductsBySwapId(swapId);

        List<SwapProductResponse> ownProducts = new ArrayList<>();
        List<SwapProductResponse> foreignProducts = new ArrayList<>();

        swapProductEntities.stream().forEach(swapProductEntity -> {
            SwapProductResponse swapProductResponse = new SwapProductResponse();
            swapProductResponse.setProductName(swapProductEntity.getUserProduct().getProduct().getName());
            swapProductResponse.setPhotos(swapProductEntity.getUserProduct().getProduct().getPhotos());
            swapProductResponse.setQuantity(swapProductEntity.getQuantity());
            if (swapProductEntity.getUserProduct().getUser().getId().equals(swap.getForeignUser().getId())) {
                foreignProducts.add(swapProductResponse);
            } else {
                ownProducts.add(swapProductResponse);
            }
        });

        Map<String, Object> response = new HashMap<>();
        SwapStatusEntity swapStatus = Arrays.stream(SwapStatusEntity.values())
                .filter(s -> s.getStatus().equals(swap.getStatus()))
                .findFirst().get();
        response.put("status", swapStatus.name().toLowerCase());
        response.put("ownProducts", ownProducts);
        response.put("foreignProducts", foreignProducts);
        response.put("createdAt", swap.getCreatedAt());

        return response;
    }
}
