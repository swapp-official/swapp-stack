package com.swapp.service.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity(name="Swap")
@NoArgsConstructor
@Table(
    name="swaps",
    indexes = {
        @Index(columnList = "own_user_id", name = "swap_own_user_idx"),
        @Index(columnList = "foreign_user_id", name = "swap_foreign_user_idx")
    }
)
public class SwapEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "own_user_id")
    private UserEntity ownUser;
    @ManyToOne()
    @JoinColumn(name = "foreign_user_id")
    private UserEntity foreignUser;
    @Column(name="own_rank")
    private Double ownRank;
    @Column(name="foreign_rank")
    private Double foreignRank;
    @Column
    private Integer status;
    @Column
    @CreatedDate
    private LocalDateTime createdAt = LocalDateTime.now();
}
