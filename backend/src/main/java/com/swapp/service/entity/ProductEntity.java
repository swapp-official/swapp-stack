package com.swapp.service.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
@Table(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "category_id")
    private CategoryEntity category;
    @Column
    private String name;
    @Column
    private String description;
    @Type(type = "list-array")
    @Column(
            name = "photos",
            columnDefinition = "text[]"
    )
    private List<String> photos;
}
