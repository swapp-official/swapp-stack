package com.swapp.service.entity;

public enum SwapStatusEntity {

    NEGOTIATED(4),
    REJECTED(3),
    ACCEPTED(2),
    PENDING(1);

    private final Integer status;

    private SwapStatusEntity(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return this.status;
    }

}
