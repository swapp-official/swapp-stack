package com.swapp.service.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name="users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String email;
    @Column
    @JsonIgnore
    private String password;
    @Column(name="user_type_id")
    private Integer userTypeId;
    @Column
    private String names;
    @Column
    private String avatar;
    @Column
    private String location;
    @CreatedDate
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @LastModifiedDate
    @Column(name="modified_at")
    private LocalDateTime modifiedAt;
    @JsonIgnore
    @Formula("(SELECT AVG(s.own_rank) FROM swaps s WHERE s.status = 2 AND s.own_user_id = id)")
    private Double rankOwn;
    @JsonIgnore
    @Formula("(SELECT AVG(s.foreign_rank) FROM swaps s WHERE s.status = 2 AND s.foreign_user_id = id)")
    private Double rankForeign;
    @JsonGetter
    private Double rank() {
        if (this.rankForeign != null && this.rankOwn != null) {
            return (this.rankOwn + this.rankForeign) / 2;
        } else if (this.rankForeign != null) {
            return this.rankForeign;
        } else if (this.rankOwn != null) {
            return this.rankOwn;
        } else {
            return null;
        }
    }
}
