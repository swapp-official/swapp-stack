package com.swapp.service.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity(name = "SwapProduct")
@NoArgsConstructor
@Table(name="swaps_products", indexes = {@Index(columnList = "swap_id")})
public class SwapProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "swap_id")
    private SwapEntity swap;
    @ManyToOne()
    @JoinColumn(name = "user_product_id")
    private UserProductEntity userProduct;
    @Column
    private Integer quantity;
    @CreatedDate
    @Column(name="created_at")
    private LocalDateTime createdAt = LocalDateTime.now();
}
