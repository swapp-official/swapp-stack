package com.swapp.service.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity(name = "UserProduct")
@Table(
    name="user_products",
    indexes = {@Index(columnList = "user_id")}
)
public class UserProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "product_id")
    private ProductEntity product;
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @Column
    private Integer quantity;
    @Column
    private String comments;
    @Column(name = "expire_date")
    private LocalDateTime expireDate;
}
