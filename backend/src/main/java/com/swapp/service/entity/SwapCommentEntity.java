package com.swapp.service.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name="swap_comments")
public class SwapCommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @ManyToOne()
    @JoinColumn(name = "swap_id")
    private SwapEntity swap;
    @Column
    private String comment;
    @CreatedDate
    @Column(name="created_at")
    private LocalDateTime createdAt = LocalDateTime.now();
}
