package com.swapp.service.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class ProductResponse {
    private Long userProductId;
    private String categoryName;
    private String productName;
    private List<String> photos;
    private Integer quantity;
    private String productDescription;
    private String userProductComments;
    private LocalDateTime userProductExpireDate;
}
