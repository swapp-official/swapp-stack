package com.swapp.service.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SwapProductResponse {
    private String productName;
    private List<String> photos;
    private Integer quantity;
}
