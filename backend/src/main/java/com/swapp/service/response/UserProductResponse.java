package com.swapp.service.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserProductResponse {
    private Long userId;
    private String userAvatar;
    private String userName;
    private List<ProductResponse> products;
}
