package com.swapp.service.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdateSwapStatusRequest {
    private String status;
}
