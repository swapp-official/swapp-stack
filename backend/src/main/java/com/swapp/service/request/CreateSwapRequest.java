package com.swapp.service.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateSwapRequest {
    private UserSwapProductsRequest own;
    private UserSwapProductsRequest foreign;
}
