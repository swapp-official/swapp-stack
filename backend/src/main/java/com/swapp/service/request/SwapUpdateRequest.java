package com.swapp.service.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SwapUpdateRequest {
    private Double rank;
    private String type;
}
