package com.swapp.service.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swapp.service.entity.SwapProductEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserSwapProductsRequest {
    private Long userId;
    @JsonProperty("products")
    private List<SwapProductEntity> swapProductEntities;
}
