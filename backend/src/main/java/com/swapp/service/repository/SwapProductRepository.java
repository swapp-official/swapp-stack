package com.swapp.service.repository;

import com.swapp.service.entity.SwapProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SwapProductRepository extends JpaRepository<SwapProductEntity, Long> {
    @Query("select s from SwapProduct s where s.swap.id = :swapId")
    public List<SwapProductEntity> listSwapProductsBySwapId(@Param("swapId") Long swapId);
}
