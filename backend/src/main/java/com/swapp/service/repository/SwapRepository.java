package com.swapp.service.repository;

import com.swapp.service.entity.SwapEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SwapRepository extends JpaRepository<SwapEntity, Long> {

    @Modifying
    @Query("UPDATE Swap s SET s.ownRank = :rank WHERE s.id = :swapId")
    public Integer updateOwnRank(@Param("swapId") Long swapId, @Param("rank") Double rank);

    @Modifying
    @Query("UPDATE Swap s SET s.foreignRank = :rank WHERE s.id = :swapId")
    public Integer updateForeignRank(@Param("swapId") Long swapId, @Param("rank") Double rank);

    @Query("SELECT s FROM Swap s WHERE (s.foreignUser.id  = :userId OR s.ownUser.id = :userId) AND status != 4")
    public List<SwapEntity> getListByUserId(@Param("userId") Long userId);

    @Modifying
    @Query("update Swap s set s.status = :status where s.id = :swapId")
    public Integer updateStatus(@Param("swapId") Long swapId, @Param("status") Integer status);
}
