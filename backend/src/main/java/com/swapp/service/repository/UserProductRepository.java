package com.swapp.service.repository;

import com.swapp.service.entity.UserEntity;
import com.swapp.service.entity.UserProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProductRepository extends JpaRepository<UserProductEntity, Long> {
    @Query("select up from UserProduct up where up.user != :user and up.quantity > 0")
    public List<UserProductEntity> getListUserProductsExcludingUser(@Param("user") UserEntity userEntity);

    @Modifying
    @Query("update UserProduct up set quantity = quantity + :amount where id = :id and quantity + :amount >= 0")
    public Integer updateQuantityByIdWithAmount(@Param("id") Long userProductId, @Param("amount") Integer amount);

    @Modifying
    @Query(value = "update user_products " +
            "set quantity = user_products.quantity - swap_product.quantity " +
            "from (" +
            "select quantity, user_product_id " +
            "from swaps_products " +
            "where swap_id = :swapId " +
            ") as swap_product " +
            "where user_products.id = swap_product.user_product_id",
            nativeQuery = true
    )
    public Integer updateProductQuantityBasedOnSwap(@Param("swapId") Long swapId);

    @Modifying
    @Query("delete from UserProduct up where up.user.id = :userId and up.quantity = 0")
    public Integer deleteProductNoQuantityByUserId(@Param("userId") Long userId);
}
