package com.swapp.service.controller;

import com.swapp.service.entity.ProductEntity;
import com.swapp.service.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public ResponseEntity<List<ProductEntity>> get(){
        return ResponseEntity.ok(productService.getProducts());
    }

    @PostMapping("/")
    public ResponseEntity<ProductEntity> add(@RequestBody ProductEntity productEntity){
        return ResponseEntity.ok(productService.createProduct(productEntity));
    }

}
