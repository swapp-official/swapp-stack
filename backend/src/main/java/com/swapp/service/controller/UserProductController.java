package com.swapp.service.controller;

import com.swapp.service.entity.UserProductEntity;
import com.swapp.service.request.UpdateUserProductRequest;
import com.swapp.service.service.UserProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-products")
public class UserProductController {
    @Autowired
    private UserProductService userProductService;

    @PutMapping("/{id}")
    public ResponseEntity<UpdateUserProductRequest> updateQuantity(
            @PathVariable(name = "id") Long userProductId,
            @RequestBody UpdateUserProductRequest payload
    ) {
        final int updated = userProductService.updateQuantityWithAmount(userProductId, payload.getAmount());

        return updated > 0
            ? ResponseEntity.ok().build()
            : ResponseEntity.badRequest().build();
    }

    @PostMapping
    public ResponseEntity<UserProductEntity> create(@RequestBody UserProductEntity userProductEntity) {
        return ResponseEntity.ok(userProductService.create(userProductEntity));
    }
}
