package com.swapp.service.controller;

import com.swapp.service.entity.UserEntity;
import com.swapp.service.response.UserProductResponse;
import com.swapp.service.service.UserProductService;
import com.swapp.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserProductService userProductService;

    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> get(@PathVariable("id") Long id){
        Optional<UserEntity> user = userService.getUser(id);
        return user.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/products")
    public ResponseEntity<List<UserProductResponse>> getUsersProducts(@RequestParam("excludedUserId") Long excludedUserId){
        return ResponseEntity.ok(userProductService.getList(excludedUserId));
    }
}
