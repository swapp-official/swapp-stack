package com.swapp.service.controller;

import com.swapp.service.entity.SwapEntity;
import com.swapp.service.entity.SwapStatusEntity;
import com.swapp.service.request.CreateSwapRequest;
import com.swapp.service.request.UpdateSwapStatusRequest;
import com.swapp.service.request.SwapUpdateRequest;
import com.swapp.service.service.SwapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import java.util.Arrays;
import java.util.Optional;

@RestController
@RequestMapping("/swaps")
public class SwapController {

    @Autowired
    private SwapService swapService;

    @PutMapping("/{id}")
    public ResponseEntity<Integer> put(@PathVariable("id") Long id, @RequestBody SwapUpdateRequest payload) {
        Integer response = swapService.updateSwapRank(id, payload.getRank(), payload.getType());
        return response > 0 ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<SwapEntity> create(@RequestBody CreateSwapRequest createSwapRequest) {
        swapService.create(createSwapRequest);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String, List<SwapEntity>>> get(@PathVariable("id") Long userId){
        Map<String, List<SwapEntity>> swapEntities = swapService.getList(userId);

        return ResponseEntity.ok(swapEntities);
    }


    @PutMapping("/{id}/change-status")
    public ResponseEntity<SwapStatusEntity> changeStatus(@PathVariable("id") Long swapId, @RequestBody UpdateSwapStatusRequest payload) {
        Optional<SwapStatusEntity> status = Arrays.stream(SwapStatusEntity.values())
            .filter(s -> s.name().equalsIgnoreCase(payload.getStatus()))
            .findFirst();

        if (!status.isPresent()) return ResponseEntity.badRequest().build();

        Integer updated = swapService.updateSwapStatus(swapId, status.get());

        return updated > 0
            ? ResponseEntity.ok().build()
            : ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).build();
    }

    @GetMapping("/{swapId}/details")
    public ResponseEntity<Map<String, Object>> getSwapDetails(@PathVariable("swapId") Long swapid) {
        return ResponseEntity.ok(swapService.getSwapDetails(swapid));
    }
}
